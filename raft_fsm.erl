%%%-------------------------------------------------------------------
%%% @author kasi-mac
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. Aug 2016 10:15 PM
%%%-------------------------------------------------------------------
-module(raft_fsm).
-author("kasi-mac").

%% API
-export([start_link/1,init/1,follower/2,candidate/2,start_raft/1]).

-record(state,{name}).

start_link(Name) ->
  gen_fsm:start_link(?MODULE, [Name], []).

init(Name) ->
  {ok, follower, #state{name=Name}}.

follower({start},S=#state{}) ->
  io:format("Server has Started ~n"),
  receive hello -> ok
  after 2000 ->
    {next_state,follower,S,2000}
  end;

follower(timeout,S) ->
  io:format("TimeOut"),
  {next_state,candidate,S,1000}.

candidate(timeout,State) ->
 io:format("Candidate Test"),
 {next_state,follower,State}.


start_raft(Pid) ->
  gen_fsm:send_event(Pid,{start}).


